use piston::window::{Window, WindowSettings};
use piston::event_loop::*;
use piston::input::*;
use graphics::types::Vec2d;
use opengl_graphics::{GlGraphics, OpenGL};
use touch_visualizer::TouchVisualizer;

#[cfg(feature = "include_sdl2")]
use sdl2_window::Sdl2Window as AppWindow;
#[cfg(feature = "include_glfw")]
use glfw_window::GlfwWindow as AppWindow;
#[cfg(feature = "include_glutin")]
use glutin_window::GlutinWindow as AppWindow;

use level::level::Level;
use inputs::Inputs;

// En cas d'erreur avec la SDL : export SDL_VIDEO_X11_VISUALID=

const OGL :OpenGL = OpenGL::V2_1;
const UPD: u64 = 60;
const SIZE: [f64; 2] = [800., 600.];
const USIZE: [u32; 2] = [SIZE[0] as u32, SIZE[1] as u32];

pub enum GState{
    OnLevel
}

/// Rast base element
pub struct Rast{
    window: AppWindow,
    gl: GlGraphics,
    state: GState,
    level: Level,
    inputs: Inputs
}

impl Rast{

    /// Create new RAST
    pub fn new()->Rast{
        Rast{
            state: GState::OnLevel,
            window:  WindowSettings::new("rast", USIZE).opengl(OGL)
                .exit_on_esc(true)
                .build().unwrap(),
            gl: GlGraphics::new(OGL),
            level: Level::new(SIZE),
            inputs: Inputs::new()
        }
    }

    pub fn run(&mut self){
        let mut tv = TouchVisualizer::new();
        let mut events = Events::new(EventSettings::new().max_fps(UPD).ups(UPD));


        while let Some(e) = events.next(&mut self.window){
            tv.event(self.window.size(), &e);

            if let Some(button) = e.press_args(){ // button press
                self.inputs.button_press(button);
            }

            if let Some(button) = e.release_args(){ // button release
                self.inputs.button_release(button);
            }

            if let Some(pt) = e.mouse_cursor_args(){ // mouse positon changed
                self.inputs.mouse_cursor(pt);
            }

            if let Some(r) = e.render_args(){ // render event
                self.render(&r);
            }

            if let Some(u) = e.update_args(){ // update event
                self.update(&u);
            }
        }
    }

    /// Render the current RAST state
    fn render(&mut self, args: &RenderArgs){
        match self.state {
            GState::OnLevel => self.level.render(args, &mut self.gl)
        }
    }

    /// update the current RAST state
    fn update(&mut self, args: &UpdateArgs){
        let size = self.window.size();

        let nstate: Option<GState>;

        match self.state {
            GState::OnLevel => {
                nstate = self.level.update(args, &self.inputs);
            }
        }

        match nstate {
            Some(gs) => self.state = gs,
            None => {}
        }
    }
}
