use piston::input::*;
use opengl_graphics::GlGraphics;
use graphics::types::{Vec2d, Color};
use rand;
use rand::Rng;
use rand::distributions::{IndependentSample, Range};
use std::cell::Cell;

use math::math::*;
use math::polygon::*;
use math::collisions::HBox;
use clipping::CPolygon;
use object::{Object, TObjectControler};
use graphs::Graphic;
use level::vessel::ammo::FAmmo;

/// Asteroid minimal area
const AREA_LOW_LIMIT: f64 = 100.;

/// Asteroid generation information
pub struct AstOpt{
    pub min_pt: u64,
    pub max_pt: u64,
    pub min_rad: f64,
    pub max_rad: f64,
    pub min_speed: f64,
    pub max_speed: f64,
    pub min_rota: f64,
    pub max_rota: f64,
    pub min_res: f64,
    pub max_res: f64
}

impl AstOpt{
    pub fn default()->AstOpt{
        AstOpt{
            min_pt: 3,
            max_pt: 10,
            min_rad: 5.,
            max_rad: 50.,
            min_speed: 20.,
            max_speed: 100.,
            min_rota: -2.,
            max_rota: 2.,
            min_res: 0.,
            max_res: 5.
        }
    }
}

/// Generate a number of points
fn rand_n_pts<R: Rng>(rng: &mut R, opt: &AstOpt)->u64{
    let r = Range::new(opt.min_pt, opt.max_pt);
    r.ind_sample(rng)
}

/// Generate a radius
fn rang_radius<R: Rng>(rng: &mut R, opt: &AstOpt)->f64{
    let r = Range::new(opt.min_rad, opt.max_rad);
    r.ind_sample(rng)
}

/// Generate the asteroid polygon (get the points and the maximum radius)
fn genpts<R: Rng>(rng: &mut R, opt: &AstOpt)->(Vec<Vec2d>, f64){
    let npts = rand_n_pts(rng, opt);
    let mut pts: Vec<Vec2d> = vec!();
    let mut max = 0.;
    let rot = TPI / npts as f64;

    for i in 0..npts{
        let agl = (i as f64) * rot;
        let rad = rang_radius(rng, opt);
        if rad > max{
            max = rad;
        }
        let pt = [agl.cos(), agl.sin()];

        pts.push(mul_scalar(pt, rad));
    }

    (pts, max)
}

/// Generate a random color
fn gencolor<R: Rng>(rng: &mut R)->Color{
    [1., 1., 1., 1.]
}

/// Generate the asteroid speed
fn genspeed<R: Rng>(rng: &mut R, opt: &AstOpt)->Vec2d{
    let r = Range::new(opt.min_speed, opt.max_speed);
    let x = r.ind_sample(rng);
    let y = r.ind_sample(rng);
    [x, y]
}

/// Generate the asteroid spawn point
fn genspawn<R: Rng>(rng: &mut R, size: &Vec2d, speed: &Vec2d, max_rad: f64)->Vec2d{
    let msx = size[0] / 2.;
    let msy = size[1] / 2.;

    // set the spawn area
    let xr;
    let xl;
    let yr;
    let yl;

    if speed[0] < 0.{
        xr = Range::new(msx, size[0]);
        xl = size[0] + max_rad;
    }else{
        xr = Range::new(0., msx);
        xl = 0. - max_rad;
    }

    if speed[1] < 0.{
        yr = Range::new(msy, size[1]);
        yl = size[1] + max_rad;
    }else{
        yr = Range::new(0., msy);
        yl = 0. - max_rad;
    }

    // random spawn point
    if rng.gen::<bool>(){ // true -> x size
        [xr.ind_sample(rng), yl]
    }else{ // y size
        [xl, yr.ind_sample(rng)]
    }
}

/// Generate a random direction
fn gendir<R: Rng>(rng: &mut R)->f64{
    Range::new(0., TPI).ind_sample(rng)
}

/// Generate a random rotation
fn genrota<R: Rng>(rng: &mut R, opt: &AstOpt)->f64{
    Range::new(opt.min_rota, opt.max_rota).ind_sample(rng)
}

/// Generate the resistance
fn genres<R: Rng>(rng: &mut R, opt: &AstOpt)->f64{
    Range::new(opt.min_res, opt.max_res).ind_sample(rng)
}

/// An asteroid
pub struct Asteroid{
    poly: Vec<Vec2d>,
    color: Color,
    on_spawn: Cell<bool>,
    obj: Object,
    res: f64
}

impl Asteroid{

    /// Create a new random asteroid
    pub fn new(size: &Vec2d, opt: &AstOpt)->Asteroid{
        let ref mut rng = rand::thread_rng();
        let (poly, max) = genpts(rng, opt);
        let color = gencolor(rng);
        let speed = genspeed(rng, opt);
        let spawn = genspawn(rng, size, &speed, opt.max_rad);
        let dir = gendir(rng);
        let rota = genrota(rng, opt);
        let res = genres(rng, opt);

        let hbox = HBox::Opti(Box::new(HBox::Circle(max)), Box::new(HBox::Polygon(poly.clone())));
        let graph = Graphic::Polygon([0., 0.], color.clone(), poly.clone()) ;

        Asteroid{
            poly: poly,
            color: color,
            on_spawn: Cell::new(true),
            obj: Object::new(spawn, speed, dir, rota, hbox, graph),
            res: res
        }
    }

    /// Make the asteroid take dammage from a projectile
    pub fn take_projectile(self, proj: &FAmmo)->Vec<Asteroid>{
        let mut nasts = vec!();

        if proj.get_dmg() > self.res{ // more dammage than the asteroid have in resistance
            let deg_rad = proj.get_dmg() - self.res;

            let apos = self.get_pos();
            let rpt = sub(proj.get_pos(), apos);
            let relpt = rotate(rpt, -self.get_direction());
            let pcircle = circle_to_polygon(deg_rad, relpt);

            // clipping polygons
            let mut cast = CPolygon::from_vec(&self.poly);
            let mut cproj = CPolygon::from_vec(&pcircle);

            for mut poly in cast.difference(&mut cproj){

                let (area, center) = area_centroid(&poly);
                if area.abs() > AREA_LOW_LIMIT{
                    trans_polygon(&mut poly, sub([0., 0.], center));

                    let npos = add(apos, center);
                    let max_rad = max_rad(&poly);

                    let hbox = HBox::Opti(Box::new(HBox::Circle(max_rad)), Box::new(HBox::Polygon(poly.clone())));
                    let graph = Graphic::Polygon([0., 0.], self.color.clone(), poly.clone());
                    let mut obj = self.obj.clone();
                    obj.set_pos(npos);
                    obj.set_hbox(hbox);
                    obj.set_graph(graph);

                    let ast = Asteroid{
                        poly: poly,
                        color: self.color.clone(),
                        on_spawn: Cell::new(false),
                        obj: obj,
                        res: self.res
                    };
                    nasts.push(ast);
                }
            }
        }else{
            nasts.push(self);
        }

        nasts
    }


    /// Render the asteroid
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        self.obj.render(args, gl)
    }

    /// Update the asteroid
    pub fn update(&self, args: &UpdateArgs, size: Vec2d){
        if self.on_spawn.get(){
            if !self.out_of_screen(size){
                self.on_spawn.set(false);
            }
        }else{
            self.obj.out_of_screen_repos(size);
        }

        self.obj.update(args);
    }
}

impl TObjectControler for Asteroid{
    fn get_pos(&self)->Vec2d{
        self.obj.get_pos()
    }

    fn set_pos(&self, pos: Vec2d){
        self.obj.set_pos(pos)
    }

    fn get_speed(&self)->Vec2d{
        self.obj.get_speed()
    }

    fn set_speed(&self, speed: Vec2d){
        self.obj.set_speed(speed)
    }

    fn get_direction(&self)->f64{
        self.obj.get_direction()
    }

    fn set_direction(&self, dir: f64){
        self.obj.set_direction(dir)
    }

    fn get_rota(&self)->f64{
        self.obj.get_rota()
    }

    fn set_rota(&self, rota: f64){
        self.obj.set_rota(rota)
    }

    fn get_hbox(&self)->&HBox{
        self.obj.get_hbox()
    }

    fn set_hbox(&mut self, hbox: HBox){
        self.obj.set_hbox(hbox)
    }

    fn get_graph(&self)->&Graphic{
        self.obj.get_graph()
    }

    fn set_graph(&mut self, graph: Graphic){
        self.obj.set_graph(graph)
    }
}
