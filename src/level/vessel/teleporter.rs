use std::cell::Cell;

use piston::input::*;
use rand;
use rand::distributions::{IndependentSample, Range};

use math::math::*;
use level::vessel::equipements::{TEquipable, Equipable, TEmplacement, Emplacement, EmplacementType};
use level::level::Level;
use inputs::Inputs;
use object::TObjectControler;

/// Teleporter informations
pub struct Teleporter{
    /* Teleporter informations */
    info: Equipable,
    /* Teleporter range */
    range: Cell<f64>,
    /* Teleporter loading time */
    load: Cell<f64>,
    /* Current load progress */
    cur_load: Cell<f64>,
    /* Teleporter reload time */
    reload: Cell<f64>,
    /* Current reload progress */
    cur_reload: Cell<f64>,
    /* load button */
    load_btn: Button
}

impl TEquipable for Teleporter{
    fn get_name(&self)->&String{
        self.info.get_name()
    }

    fn get_descr(&self)->&String{
        self.info.get_descr()
    }

    fn get_type(&self)->&EmplacementType{
        self.info.get_type()
    }

    fn update(&self, args: &UpdateArgs, inputs: &Inputs, level: &Level, _: &TEmplacement<Self>){
        // update reload timer
        self.cur_reload.set(self.cur_reload.get() - args.dt);
        if self.cur_reload.get() < 0.{
            self.cur_reload.set(0.);
        }

        // load the teleporter
        if inputs.is_pressed(&self.load_btn) && self.cur_reload.get() == 0.{
            self.cur_load.set(self.cur_load.get() + args.dt);

            if self.cur_load.get() >= self.load.get(){ // teleportation
                self.cur_load.set(0.);
                self.cur_reload.set(self.reload.get());

                // generate new position in range
                let ref mut rng = rand::thread_rng();
                let rot = Range::new(0., TPI).ind_sample(rng);
                let rad = Range::new(0., self.range.get()).ind_sample(rng);
                let rpt = angle_to_vec(rot, rad);

                let npos = mod_vec(add(level.get_vessel().get_pos(), rpt), level.get_size());
                level.get_vessel().set_pos(npos);
            }
        }else{
            self.cur_load.set(0.);
        }
    }
}

impl Teleporter{

    /// Small teleporter
    pub fn small()->Teleporter{
        Teleporter{
            info: Equipable::new("Small teleporter".to_string(), "A small and inefficient teleporter".to_string(), EmplacementType::from(vec!["control", "teleporter"])),
            range: Cell::new(200.),
            load: Cell::new(5.),
            cur_load: Cell::new(0.),
            reload: Cell::new(60.),
            cur_reload: Cell::new(0.),
            load_btn: Button::Keyboard(Key::A)
        }
    }
}

/// An emplacement for a teleporter
pub type TeleporterEmplacement = Emplacement<Teleporter>;
