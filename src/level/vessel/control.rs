use std::cell::Cell;

use piston::input::*;

use level::vessel::equipements::{TEquipable, Equipable, TEmplacement, Emplacement, EmplacementType};
use math::math::*;
use object::TObjectControler;
use level::level::Level;
use inputs::Inputs;

/// Orientation controler
pub struct Control{
    /* Control informations */
    info: Equipable,
    /* rotation speed */
    speed: Cell<f64>,
    /* target orientation */
    target: Cell<f64>,
    /* current orientation */
    current: Cell<f64>
}

impl TEquipable for Control{
    fn get_name(&self)->&String{
        self.info.get_name()
    }

    fn get_descr(&self)->&String{
        self.info.get_descr()
    }

    fn get_type(&self)->&EmplacementType{
        self.info.get_type()
    }

    fn update(&self, _: &UpdateArgs, inputs: &Inputs, level: &Level, _: &TEmplacement<Self>){
        let pt = inputs.get_pt();
        let pos = level.get_vessel().get_pos();

        let rpt = sub(pt, pos);
        let dir = rpt[1].atan2(rpt[0]);

        level.get_vessel().set_direction(dir);
    }
}

impl Control{

    /// The basic controler
    pub fn basic()->Control{
        Control{
            info: Equipable::new("Basic controler".to_string(), "A simple slow controler".to_string(), EmplacementType::from(vec!["control", "controler"])),
            speed: Cell::new(TPI),
            target: Cell::new(0.),
            current: Cell::new(0.)
        }
    }
}

/// An emplacement for a control system
pub type ControlEmplacement = Emplacement<Control>;
