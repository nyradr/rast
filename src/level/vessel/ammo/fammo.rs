use piston::input::*;
use opengl_graphics::GlGraphics;
use graphics::math::*;

use object::{Object, TObjectControler};
use graphs::Graphic;
use math::collisions::HBox;

/// A fired ammo
pub struct FAmmo{
    obj: Object,
    dammage: f64
}

impl FAmmo{
    /// Create a fired ammo from a position, a speed, a direction and ammo informations
    pub fn new(pos: Vec2d, speed: Vec2d, dir: f64, obj: &Object, dmg: f64)->FAmmo{
        let nobj = (*obj).clone();

        nobj.set_pos(pos);
        nobj.set_speed(speed);
        nobj.set_direction(dir);

        FAmmo{
            obj: nobj,
            dammage: dmg
        }
    }

    /// Render the ammo
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        self.obj.render(args, gl);
    }

    /// Update the ammo
    pub fn update(&self, args: &UpdateArgs){
        self.obj.update(args);
    }

    pub fn get_dmg(&self)->f64{
        self.dammage
    }
}

impl TObjectControler for FAmmo{
    fn get_pos(&self)->Vec2d{
        self.obj.get_pos()
    }

    fn set_pos(&self, pos: Vec2d){
        self.obj.set_pos(pos)
    }

    fn get_speed(&self)->Vec2d{
        self.obj.get_speed()
    }

    fn set_speed(&self, speed: Vec2d){
        self.obj.set_speed(speed)
    }

    fn get_direction(&self)->f64{
        self.obj.get_direction()
    }

    fn set_direction(&self, dir: f64){
        self.obj.set_direction(dir)
    }

    fn get_rota(&self)->f64{
        self.obj.get_rota()
    }

    fn set_rota(&self, rota: f64){
        self.obj.set_rota(rota)
    }

    fn get_hbox(&self)->&HBox{
        self.obj.get_hbox()
    }

    fn set_hbox(&mut self, hbox: HBox){
        self.obj.set_hbox(hbox)
    }

    fn get_graph(&self)->&Graphic{
        self.obj.get_graph()
    }

    fn set_graph(&mut self, graph: Graphic){
        self.obj.set_graph(graph)
    }
}
