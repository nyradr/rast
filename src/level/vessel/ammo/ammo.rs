use piston::input::UpdateArgs;
use graphics::math::*;
use graphics::types::Vec2d;

use object::{Object, TObjectControler};
use level::vessel::equipements::{TEquipable, Equipable, TEmplacement, EmplacementType};
use graphs::Graphic;
use math::collisions::HBox;

use level::level::Level;
use level::vessel::ammo::FAmmo;
use inputs::Inputs;

/// Ammunition functions
pub trait TAmmo{

    /// shoot an ammo
    fn shoot(&self, pos: Vec2d, vspeed: Vec2d, dir: f64)->FAmmo;
}

/// Ammunition informations
pub struct Ammo{
    info: Equipable,
    obj: Object,
    dammage: f64
}

impl Ammo{
    pub fn bullet()->Self{
        let radius = 1.;
        let hbox = HBox::Circle(radius);
        let speed = [500., 500.];
        let graph = Graphic::Circle([0., 0.], [1.0, 0.0, 0.0, 1.0], radius);

        Ammo{
            info: Equipable::new("Simple bullet".to_string(), "A simple bullet".to_string(), EmplacementType::from(vec!["ammo", "gun"])),
            obj: Object::new([0., 0.], speed, 0., 0., hbox, graph),
            dammage: 10.
        }
    }
}

impl TEquipable for Ammo{
    fn get_name(&self)->&String{
        self.info.get_name()
    }

    fn get_descr(&self)->&String{
        self.info.get_descr()
    }

    fn get_type(&self)->&EmplacementType{
        self.info.get_type()
    }

    fn update(&self, _: &UpdateArgs, _: &Inputs, _: &Level, _: &TEmplacement<Self>){

    }
}

impl TAmmo for Ammo{
    fn shoot(&self, pos: Vec2d, vspeed: Vec2d, dir: f64)->FAmmo{
        let mut speed = add(vspeed, self.obj.get_speed());
        speed = [speed[0] * dir.cos(), speed[1] * dir.sin()];
        FAmmo::new(pos, speed, dir, &self.obj, self.dammage)
    }
}
