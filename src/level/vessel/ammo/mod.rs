mod ammo;
mod fammo;

pub use self::ammo::TAmmo;
pub use self::ammo::Ammo;
pub use self::fammo::FAmmo;

use level::vessel::equipements::Emplacement;

/// Emplacement for ammo
pub type AmmoEmplacement = Emplacement<Ammo>;
