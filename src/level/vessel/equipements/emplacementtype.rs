/// Emplacement type of an equipable object
pub enum EmplacementType{
    /// No type
    Empty,
    /// Sub type (increase presision)
    Sub(String, Box<EmplacementType>)
}

impl EmplacementType{
    pub fn from(mut v: Vec<&str>)->Self{
        if v.is_empty(){
            EmplacementType::Empty
        }else{
            let l = v.remove(0).to_string();
            let r = EmplacementType::from(v);
            EmplacementType::Sub(l, Box::new(r))
        }
    }
}

/// Emplacement types allowed for an emplacement.
/// A set of rules to define types allowed (or not) for an emplacement
pub enum EmplacementTypeAuth{
    Base(String),
    Sub(String, Box<EmplacementTypeAuth>),
    Or(Box<EmplacementTypeAuth>, Box<EmplacementTypeAuth>)
}

impl EmplacementTypeAuth{

    /// Test the compactibility of an emplacement type with an emplacement authorisation.
    /// A more precise type is compactible with this authorisation.
    /// Return true if the type is compactible.
    pub fn compactible(&self, etype: &EmplacementType)->bool{
        match self{
            &EmplacementTypeAuth::Base(ref name) => match etype{
                &EmplacementType::Empty => false,
                &EmplacementType::Sub(ref ename, _) => name == ename
            },
            &EmplacementTypeAuth::Sub(ref name, ref sub) => match etype{
                &EmplacementType::Empty => false,
                // same with sub
                &EmplacementType::Sub(ref ename, ref esub) => {
                    if name == ename{
                        sub.compactible(esub)
                    }else{
                        false
                    }
                }
            },
            &EmplacementTypeAuth::Or(ref a, ref b) =>
                a.compactible(etype) || b.compactible(etype)
        }
    }
}
