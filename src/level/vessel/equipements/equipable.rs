use piston::input::UpdateArgs;
use level::vessel::equipements::{TEmplacement, EmplacementType};
use level::level::Level;
use inputs::Inputs;

/// Equipable functions
pub trait TEquipable{

    /// Get the equipement name
    fn get_name(&self)->&String;

    /// Get the equipement description
    fn get_descr(&self)->&String;

    /// Get the equipement type
    fn get_type(&self)->&EmplacementType;

    /// Update the equipement in function of the inputs
    fn update(&self, args: &UpdateArgs, inputs: &Inputs, level: &Level, emplacement: &TEmplacement<Self>);
}

/// A piece of equipement
pub struct Equipable{
    /// Equipement name
    name: String,
    /// Equipement description
    descr: String,
    /// Equipement type
    etype: EmplacementType
}

impl Equipable{
    pub fn new(name: String, descr: String, etype: EmplacementType)->Equipable{
        Equipable{
            name: name,
            descr: descr,
            etype: etype
        }
    }
}

impl TEquipable for Equipable{
    fn get_name(&self)->&String{
        &self.name
    }

    fn get_descr(&self)->&String{
        &self.descr
    }

    fn get_type(&self)->&EmplacementType{
        &self.etype
    }

    fn update(&self, _: &UpdateArgs, _: &Inputs, _: &Level, _: &TEmplacement<Self>){
        // Do nothing
    }
}
