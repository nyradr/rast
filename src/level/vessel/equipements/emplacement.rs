use graphics::types::Vec2d;
use piston::input::UpdateArgs;

use level::vessel::equipements::{TEquipable, EmplacementTypeAuth};
use level::level::Level;
use inputs::Inputs;

pub trait TEmplacement<T: TEquipable>{
    /// Test if the emplacement can equip this equipement
    fn can_equip(&self, e: &T)->bool;

    /// Equip (if possible) an equipement
    fn equip(&mut self, e: T)->bool;

    /// Test if the emplacement contains an element
    fn has_element(&self)->bool;

    /// Get the current element
    fn get_elem(&self)->Option<&T>;

    /// Get the current element as mutable
    fn get_elem_mut(&mut self)->Option<&mut T>;

    /// Get the allowed types
    fn get_allowed(&self)->&EmplacementTypeAuth;

    /// Get the emplacement relative position
    fn get_pos(&self)->Vec2d;

    /// Update equipement inside an emplacement
    fn update(&self, args: &UpdateArgs, inputs: &Inputs, level: &Level);
}

/// Emplacement of something equipable
pub struct Emplacement<T: TEquipable>{
    /// Equipement used
    element: Option<T>,
    /// Allowed types for this emplacement
    allowed: EmplacementTypeAuth,
    /// Emplacement position, relative to the emplacement container
    pos: Vec2d
}

impl<T: TEquipable> Emplacement<T>{

    /// Create empty emplacement
    pub fn new(allowed: EmplacementTypeAuth, pos: Vec2d)->Emplacement<T>{
        Emplacement{
            element: None,
            allowed: allowed,
            pos: pos
        }
    }
}

impl<T: TEquipable> TEmplacement<T> for Emplacement<T>{
    fn can_equip(&self, e: &T)->bool{
        self.allowed.compactible(e.get_type())
    }

    fn equip(&mut self, e: T)->bool{
        let c = self.can_equip(&e);
        if c{
            self.element = Some(e);
        }
        c
    }

    fn has_element(&self)->bool{
        match self.element {
            Some(_) => true,
            None => false
        }
    }

    fn get_elem(&self)->Option<&T>{
        match self.element{
            Some(ref e) => Some(e),
            None => None
        }
    }

    fn get_elem_mut(&mut self)->Option<&mut T>{
        match self.element{
            Some(ref mut e) => Some(e),
            None => None
        }
    }

    fn get_allowed(&self)->&EmplacementTypeAuth{
        &self.allowed
    }

    fn get_pos(&self)->Vec2d{
        self.pos
    }

    fn update(&self, args: &UpdateArgs, inputs: &Inputs, level: &Level){
        match self.get_elem(){
            Some(e) => e.update(args, inputs, level, self),
            None => {}
        }
    }
}
