use std::cell::Cell;

use piston::input::*;
use graphics::types::Vec2d;

use object::{Object, TObjectControler};
use graphs::Graphic;
use math::collisions::HBox;
use math::math::*;

use level::level::Level;
use level::vessel::ammo::{TAmmo, FAmmo, Ammo, AmmoEmplacement};
use level::vessel::equipements::{TEquipable, Equipable, TEmplacement, Emplacement, EmplacementType, EmplacementTypeAuth};
use inputs::Inputs;

/// A weapon
pub struct Weapon{
    info: Equipable,
    obj: Object,
    ammo: AmmoEmplacement,
    rate: Cell<f64>,
    cur_rate: Cell<f64>,
    fire_btn: Button
}

impl TEquipable for Weapon{
    fn get_name(&self)->&String{
        self.info.get_name()
    }

    fn get_descr(&self)->&String{
        self.info.get_descr()
    }

    fn get_type(&self)->&EmplacementType{
        self.info.get_type()
    }

    fn update(&self, args: &UpdateArgs, inputs: &Inputs, level: &Level, emplacement: &TEmplacement<Self>){
        let mut cur_rate = self.get_cur_rate();

        if inputs.is_pressed(&self.fire_btn){
            if cur_rate == 0.{
                let vessel = level.get_vessel();
                let dir = vessel.get_direction();

                let epos = abso(emplacement.get_pos(), vessel.get_pos(), dir);
                let npos = abso(self.obj.get_pos(), epos, dir);

                self.set_cur_rate(self.get_rate());
                match self.ammo.get_elem(){
                    Some(am) => level.add_projectile(am.shoot(npos, vessel.get_speed(), dir)),
                    None => {}
                }
            }
        }

        if cur_rate > 0.{
            cur_rate -= args.dt;
            if cur_rate < 0.{
                cur_rate = 0.;
            }

            self.set_cur_rate(cur_rate);
        }
    }
}

impl Weapon{

    pub fn gun()->Weapon{
        let mut ammo = AmmoEmplacement::new(
            EmplacementTypeAuth::Sub(
                "ammo".to_string(),
                Box::new(EmplacementTypeAuth::Base("gun".to_string()))
            ),
            [0., 0.]);
        ammo.equip(Ammo::bullet());

        Weapon{
            info: Equipable::new("gun".to_string(), "a gun".to_string(), EmplacementType::from(vec!["weapon", "gun"])),
            obj: Object::new([0., 0.], [0., 0.], 0., 0., HBox::None, Graphic::None),
            ammo: ammo,
            rate: Cell::new(0.2),
            cur_rate: Cell::new(0.),
            fire_btn: Button::Mouse(MouseButton::Left)
        }
    }

    /// Get the firing rage (cooldown in ms before the next shoot)
    pub fn get_rate(&self)->f64{
        self.rate.get()
    }

    /// Get the cooldown state
    pub fn get_cur_rate(&self)->f64{
        self.cur_rate.get()
    }

    /// Set the cooldown value
    pub fn set_cur_rate(&self, r: f64){
        self.cur_rate.set(r)
    }
}

/// An emplacement for weapons
pub type WeaponEmplacement = Emplacement<Weapon>;
