use std::cell::{Cell, RefCell};

use piston::input::*;
use opengl_graphics::GlGraphics;

use math::math::*;
use level::vessel::equipements::{TEquipable, Equipable, TEmplacement, Emplacement, EmplacementType};
use level::effects::{TEffect, EngineBurnEffect};
use level::level::Level;
use object::TObjectControler;
use inputs::Inputs;

/// Engine informations
pub struct Engine{
    /* Equipement informations */
    info: Equipable,
    /* engine effects */
    effects: RefCell<EngineBurnEffect>,
    /* Engine max trust */
    trust: Cell<f64>,
    /* Engine current fuel */
    fuel: Cell<f64>,
    /* Max fuel for the engine */
    fuel_max: Cell<f64>,
    /* Engine consumtion */
    consum: Cell<f64>,
    /* Trust percentage */
    on_pc: Cell<f64>,
    /* Percentage change */
    pc: Cell<f64>,
    /* Engine activation button */
    on: Button
}

impl TEquipable for Engine{
    fn get_name(&self)->&String{
        self.info.get_name()
    }

    fn get_descr(&self)->&String{
        self.info.get_descr()
    }

    fn get_type(&self)->&EmplacementType{
        self.info.get_type()
    }

    fn update(&self, args: &UpdateArgs, inputs: &Inputs, level: &Level, emplacement: &TEmplacement<Self>){
        // update engine state
        if inputs.is_pressed(&self.on){
            self.on_pc.set(self.on_pc.get() + self.pc.get() * args.dt);
            if self.on_pc.get() > 1.{
                self.on_pc.set(1.);
            }

            if self.fuel.get() > 0.{
                self.fuel.set(self.fuel.get() - self.consum.get() * self.on_pc.get() * args.dt);

                let pos = abso(emplacement.get_pos(), level.get_vessel().get_pos(), level.get_vessel().get_direction());

                self.effects.borrow_mut().spawn(pos, 0.);
            }
        }else{
            self.on_pc.set(self.on_pc.get() - self.pc.get() * args.dt);
            if self.on_pc.get() < 0.{
                self.on_pc.set(0.);
            }
        }

        let cos = level.get_vessel().get_direction().cos() * args.dt;
        let sin = level.get_vessel().get_direction().sin() * args.dt;

        let acc = self.get_power();
        level.get_vessel().accelerate([acc * cos, acc * sin]);

        self.effects.borrow_mut().update(args);
    }
}

impl Engine{

    /// Main engine
    pub fn main()->Engine{
        let eff = EngineBurnEffect::new(5., [1., 0.25, 0., 1.], 0., [1., 1., 0., 1.], 0.1);

        Engine{
            info: Equipable::new("Main engine".to_string(), "The basic engine".to_string(), EmplacementType::from(vec!["engine", "main"])),
            effects: RefCell::new(eff),
            trust: Cell::new(100.),
            fuel: Cell::new(1000.),
            fuel_max: Cell::new(10.),
            consum: Cell::new(0.5),
            on_pc: Cell::new(0.),
            pc: Cell::new(1.),
            on: Button::Keyboard(Key::Z)
        }
    }

    pub fn postcomb()->Engine{
        let eff = EngineBurnEffect::new(10., [1., 0.25, 0., 1.], 0., [1., 1., 0., 1.], 0.13);

        Engine{
            info: Equipable::new("Post burn".to_string(), "Short but powerfull".to_string(), EmplacementType::from(vec!["engine", "pc"])),
            effects: RefCell::new(eff),
            trust: Cell::new(500.),
            fuel: Cell::new(10.),
            fuel_max: Cell::new(10.),
            consum: Cell::new(1.),
            on_pc: Cell::new(0.),
            pc: Cell::new(2.),
            on: Button::Keyboard(Key::LShift)
        }
    }

    /// Render the effects
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        self.effects.borrow_mut().render(args, gl);
    }

    /// Get the engine power (if on)
    pub fn get_power(&self)->f64{
        if self.fuel.get() > 0.{
            self.trust.get() * self.on_pc.get()
        }else{
            0.
        }
    }

    /// Get the fuel left in the engine
    pub fn get_fuel_left(&self)->f64{
        self.fuel.get()
    }

    /// Get the fuel max of the engine
    pub fn get_fuel_max(&self)->f64{
        self.fuel_max.get()
    }

    /// Get the engine consumtion
    pub fn get_consum(&self)->f64{
        self.consum.get()
    }

    /// Is the engine on
    pub fn get_on_pc(&self)->f64{
        self.on_pc.get()
    }

    /// Get the engine percentage
    pub fn get_pc(&self)->f64{
        self.pc.get()
    }
}

/// Emplacement for an engine
pub type EngineEmplacement = Emplacement<Engine>;

impl EngineEmplacement{
    /// Render the engine effetcs (if there is an engine)
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        match self.get_elem_mut(){
            Some(e) => e.render(args, gl),
            None => {}
        }
    }
}
