use piston::input::*;
use opengl_graphics::GlGraphics;
use graphics::types::Vec2d;

use object::{Object, TObjectControler};
use math::collisions::HBox;
use graphs::Graphic;
use level::vessel::engine::{Engine, EngineEmplacement};
use level::vessel::weapon::{Weapon, WeaponEmplacement};
use level::vessel::ammo::FAmmo;
use level::vessel::teleporter::{Teleporter, TeleporterEmplacement};
use level::vessel::control::{Control, ControlEmplacement};
use level::vessel::equipements::{EmplacementTypeAuth, TEmplacement};
use inputs::Inputs;
use level::level::Level;

/// Player vessel
pub struct Vessel{
    /* Basic informations */
    obj: Object,
    /* Main engine */
    engine_main: EngineEmplacement,
    /* Burst engine */
    engine_pc: EngineEmplacement,
    /* first weapon */
    weapon_first: WeaponEmplacement,
    /* teleporter */
    teleporter: TeleporterEmplacement,
    /* direction controler */
    controler: ControlEmplacement
}

impl Vessel{

    /// Create a new vessel
    pub fn new(pos: Vec2d)->Vessel{
        let poly = vec![[-10., -10.], [-10., 10.], [20., 0.]];
        let hbox = HBox::Polygon(poly.clone());
        let graph = Graphic::Polygon([0., 0.], [1.0, 0.0, 0.0, 1.0], poly.clone());

        Vessel{
            obj: Object::new(pos, [0., 0.], 0., 0., hbox, graph),
            engine_main: EngineEmplacement::new(
                EmplacementTypeAuth::Sub(
                    "engine".to_string(),
                    Box::new(EmplacementTypeAuth::Base("main".to_string()))
                ),
                [-10., 0.]
            ),
            engine_pc: EngineEmplacement::new(
                EmplacementTypeAuth::Sub(
                    "engine".to_string(),
                    Box::new(EmplacementTypeAuth::Base("pc".to_string()))
                ),
                [-10., 0.]),
            weapon_first: WeaponEmplacement::new(
                EmplacementTypeAuth::Sub(
                    "weapon".to_string(),
                    Box::new(EmplacementTypeAuth::Base("gun".to_string()))
                ),
                [20., 0.]),
            teleporter: TeleporterEmplacement::new(
                EmplacementTypeAuth::Sub(
                    "control".to_string(),
                    Box::new(EmplacementTypeAuth::Base("teleporter".to_string()))
                ),
                [0., 0.]),
            controler: ControlEmplacement::new(
                EmplacementTypeAuth::Sub(
                    "control".to_string(),
                    Box::new(EmplacementTypeAuth::Base("controler".to_string()))
                ),
                [0., 0.])
        }
    }

    /// Equip an engine as a main engine
    pub fn set_main_engine(&mut self, e: Engine)->bool{
        self.engine_main.equip(e)
    }

    /// Equip an engine as burst engine
    pub fn set_pc_engine(&mut self, e: Engine)->bool{
        self.engine_pc.equip(e)
    }

    /// Equip a primary weapon
    pub fn set_first_weapon(&mut self, w: Weapon)->bool{
        self.weapon_first.equip(w)
    }

    /// Equip a teleporter
    pub fn set_teleporter(&mut self, t: Teleporter)->bool{
        self.teleporter.equip(t)
    }

    pub fn set_controler(&mut self, c: Control)->bool{
        self.controler.equip(c)
    }

    /// Render the vessel
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        self.engine_main.render(args, gl);
        self.engine_pc.render(args, gl);

        self.obj.render(args, gl);
    }

    /// Update the vessel
    pub fn update(&self, args: &UpdateArgs, level: &Level, inputs: &Inputs){
        self.engine_main.update(args, inputs, level);
        self.engine_pc.update(args, inputs, level);

        self.weapon_first.update(args, inputs, level);

        self.teleporter.update(args, inputs, level);
        self.controler.update(args, inputs, level);

        self.obj.out_of_screen_repos(level.get_size());
        self.obj.update(args);
    }
}

impl TObjectControler for Vessel{
    fn get_pos(&self)->Vec2d{
        self.obj.get_pos()
    }

    fn set_pos(&self, pos: Vec2d){
        self.obj.set_pos(pos)
    }

    fn get_speed(&self)->Vec2d{
        self.obj.get_speed()
    }

    fn set_speed(&self, speed: Vec2d){
        self.obj.set_speed(speed)
    }

    fn get_direction(&self)->f64{
        self.obj.get_direction()
    }

    fn set_direction(&self, dir: f64){
        self.obj.set_direction(dir)
    }

    fn get_rota(&self)->f64{
        self.obj.get_rota()
    }

    fn set_rota(&self, rota: f64){
        self.obj.set_rota(rota)
    }

    fn get_hbox(&self)->&HBox{
        self.obj.get_hbox()
    }

    fn set_hbox(&mut self, hbox: HBox){
        self.obj.set_hbox(hbox)
    }

    fn get_graph(&self)->&Graphic{
        self.obj.get_graph()
    }

    fn set_graph(&mut self, graph: Graphic){
        self.obj.set_graph(graph)
    }
}
