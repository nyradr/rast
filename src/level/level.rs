use std::cell::{Cell, RefCell};

use piston::input::*;
use opengl_graphics::GlGraphics;
use graphics::*;
use graphics::types::Vec2d;

use object::TObjectControler;
use level::vessel::vessel::Vessel;
use level::asteroid::{Asteroid, AstOpt};
use level::vessel::ammo::FAmmo;
use level::vessel::engine::Engine;
use level::vessel::weapon::Weapon;
use level::vessel::teleporter::Teleporter;
use level::vessel::control::Control;
use inputs::Inputs;

use rast::GState;

/// Game level
pub struct Level{
    /* background color */
    background: [f32; 4],
    /* Player vessel */
    vessel: Vessel,
    /* Asteroids generation options */
    ast_options: AstOpt,
    /* Asteroids */
    asteroids: RefCell<Vec<Asteroid>>,
    /* projectiles shooted */
    projectiles: RefCell<Vec<FAmmo>>,
    /* Level size */
    size: Vec2d,
    /* Asteroid spawn timer */
    ast_spawn_timer: Cell<f64>,
    /* Spawn delay */
    ast_spawn_delay: Cell<f64>
}

impl Level{

    pub fn new(size: types::Vec2d)->Level{
        let mut asts:Vec<Asteroid> = vec!();
        let opt = AstOpt::default();
        for _ in 0..10{
            asts.push(Asteroid::new(&size, &opt));
        }

        let mut vessel = Vessel::new([size[0] / 2., size[1] / 2.]);
        vessel.set_main_engine(Engine::main());
        vessel.set_pc_engine(Engine::postcomb());
        vessel.set_first_weapon(Weapon::gun());
        vessel.set_teleporter(Teleporter::small());
        vessel.set_controler(Control::basic());

        Level{
            background: [0., 0., 0., 1.],
            vessel: vessel,
            ast_options: opt,
            asteroids: RefCell::new(asts),
            projectiles: RefCell::new(vec!()),
            size: size,
            ast_spawn_timer: Cell::new(10.),
            ast_spawn_delay: Cell::new(10.)
        }
    }

    /// Render the level
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        gl.draw(args.viewport(), |c, gl|{
            clear(self.background, gl);
        });

        // Render the asteroids
        for ref mut a in self.asteroids.borrow_mut().iter_mut(){
            a.render(args, gl);
        }

        // Render the projectiles
        for ref mut p in self.projectiles.borrow_mut().iter_mut(){
            p.render(args, gl);
        }

        self.vessel.render(args, gl);
    }

    /// Manage the collision of a projectile with the asteroids. Return true if there is a collision
    fn proj_asteroids_collision(&self, ip: usize)->bool{
        let mut i = 0;
        let ref p = self.projectiles.borrow()[ip];
        let mut asteroids = self.asteroids.borrow_mut();

        while i < asteroids.len(){

            if p.collide(&asteroids[i]){
                let ast = asteroids.remove(i);
                let nasts = ast.take_projectile(p);

                for a in nasts{
                    asteroids.push(a);
                }

                return true;
            }else{
                i += 1;
            }
        }

        false
    }

    /// Manage the collision of the projectiles with the asteroids
    fn projs_ast_collisions(&self){
        let mut i = 0;

        while i < self.projectiles.borrow().len(){
            if self.proj_asteroids_collision(i){
                // remove the projectile in case of collision
                self.projectiles.borrow_mut().remove(i);
            }else{
                i += 1;
            }
        }
    }

    /// Update the level
    pub fn update(&self, args: &UpdateArgs, inputs: &Inputs)->Option<GState>{
        // Update asteroids
        for ref mut a in self.asteroids.borrow().iter(){
            a.update(args, self.size);
        }

        // Update projectiles (an clean out of screen ones)
        let mut i = 0;
        while i < self.projectiles.borrow().len(){
            if self.projectiles.borrow()[i].out_of_screen(self.size){
                self.projectiles.borrow_mut().remove(i);
            }else{
                self.projectiles.borrow()[i].update(args);
                i += 1;
            }
        }

        self.projs_ast_collisions();

        self.vessel.update(args, self, inputs);

        // handle asteroids spawn
        self.ast_spawn(args);

        None
    }

    /// Handle the spawn of new asteroids
    fn ast_spawn(&self, args: &UpdateArgs){
        self.ast_spawn_timer.set(self.ast_spawn_timer.get() - args.dt);
        if (self.ast_spawn_timer.get() <= 0.){
            let delay = self.ast_spawn_delay.get() * 0.98; // TODO fix value
            self.ast_spawn_delay.set(delay);
            self.ast_spawn_timer.set(delay);

            let asteroid = Asteroid::new(&self.size, &self.ast_options);
            self.asteroids.borrow_mut().push(asteroid);
        }
    }

    /// Get the level size
    pub fn get_size(&self)->Vec2d{
        self.size
    }

    /// Get the player vessel
    pub fn get_vessel(&self)->&Vessel{
        &self.vessel
    }

    /// Add a projectile to the projectile list
    pub fn add_projectile(&self, proj: FAmmo){
        self.projectiles.borrow_mut().push(proj);
    }
}
