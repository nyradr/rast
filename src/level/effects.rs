use piston::input::*;
use opengl_graphics::GlGraphics;
use graphics::types::{Vec2d, Color};
use std::collections::LinkedList;

use graphs::Graphic;

/// Effect functions
pub trait TEffect{

    /// Make spawn the effect
    fn spawn(&mut self, pos: Vec2d, dir: f64);

    /// Render the effect
    fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics);

    /// Update the effect
    fn update(&mut self, args: &UpdateArgs);
}

/// Effect representing an engine burn
pub struct EngineBurnEffect{
    /* graphical objects */
    graphs: LinkedList<(Graphic, Vec2d, f64, f64)>,
    /* effect start radius */
    start_radius: f64,
    /* effect start color */
    start_color: Color,
    /* effect end radius */
    end_radius: f64,
    /* effect en color */
    end_color: Color,
    /* duration of the effect */
    duration: f64,
}

impl EngineBurnEffect{
    pub fn new(start_radius: f64, start_color: Color, end_radius: f64, end_color: Color, duration: f64)->EngineBurnEffect{
        EngineBurnEffect{
            graphs: LinkedList::new(),
            start_radius: start_radius,
            start_color: start_color,
            end_radius: end_radius,
            end_color: end_color,
            duration: duration
        }
    }
}

impl TEffect for EngineBurnEffect{

    fn spawn(&mut self, pos: Vec2d, dir: f64){
        let graph = Graphic::Circle([0., 0.], self.start_color, self.start_radius);
        self.graphs.push_back((graph, pos, dir, 0.));
    }

    fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        for ref mut g in self.graphs.iter_mut(){
            g.0.render(args, gl, &g.1, &g.2);
        }
    }

    fn update(&mut self, args: &UpdateArgs){
        // delete expired effects
        let mut ctn = true;
        while ctn{
            match self.graphs.pop_front(){
                Some(g) =>{
                    if g.3 < self.duration{
                        self.graphs.push_front(g);
                        ctn = false;
                    }
                },
                None =>{
                    ctn = false;
                }
            }
        }
        drop(ctn);

        // update circle size informations
        let el = self.duration / args.dt;
        let dr = (self.start_radius - self.end_radius) / el;

        // update color informations
        let vcl = [
            (self.end_color[0] - self.start_color[0]) / el as f32,
            (self.end_color[1] - self.start_color[1]) / el as f32,
            (self.end_color[2] - self.start_color[2]) / el as f32,
            (self.end_color[3] - self.start_color[3]) / el as f32,
        ];

        // update remaining elements
        for ref mut g in self.graphs.iter_mut(){
            match g.0{
                // update circle
                Graphic::Circle(p, col, r) =>{
                    let nr = r - dr;
                    let nc = [
                        col[0] + vcl[0],
                        col[1] + vcl[1],
                        col[2] + vcl[2],
                        col[3] + vcl[3]
                    ];

                    (g.0) = Graphic::Circle(p, nc, nr);
                },
                _ => {}
            }

            // update lifetime
            g.3 += args.dt;
        }
    }
}
