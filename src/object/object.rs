use std::cell::Cell;

use graphics::types::Vec2d;
use graphics::math::*;
use piston::input::*;
use opengl_graphics::GlGraphics;

use math::collisions::{HBox, collide, out_of_screen};
use graphs::Graphic;

/// Control an object movement in space
pub trait TObjectControler{
    /// Get the object position
    fn get_pos(&self)->Vec2d;

    /// Set the object position
    fn set_pos(&self, pos: Vec2d);

    /// Get the object speed
    fn get_speed(&self)->Vec2d;

    /// Set the object speed
    fn set_speed(&self, speed: Vec2d);

    /// Accelerate the object speed
    fn accelerate(&self, acc: Vec2d){
        let mut speed = self.get_speed();
        speed[0] += acc[0];
        speed[1] += acc[1];
        self.set_speed(speed)
    }

    /// Get the object direction
    fn get_direction(&self)->f64;

    /// Set the object direction
    fn set_direction(&self, dir: f64);

    /// Get the object rotation speed
    fn get_rota(&self)->f64;

    /// Set the object rotation
    fn set_rota(&self, rota: f64);

    /// Get the object hitbox
    fn get_hbox(&self)->&HBox;

    /// Set the object hitbox
    fn set_hbox(&mut self, hbox: HBox);

    fn collide(&self, other: &TObjectControler)->bool{
        collide(
            self.get_hbox(),
            self.get_pos(),
            self.get_direction(),
            other.get_hbox(),
            other.get_pos(),
            other.get_direction()
        )
    }

    /// Get the graphics of the object
    fn get_graph(&self)->&Graphic;

    /// Set the object graphics
    fn set_graph(&mut self, graph: Graphic);

    /// Test if the object is out of the screen
    fn out_of_screen(&self, size: Vec2d)->bool{
        out_of_screen(
            self.get_hbox(),
            self.get_pos(),
            size
        )
    }

    /// Test if the object is out of screen and guive a new speed and position
    fn out_of_screen_repos(&self, size: Vec2d){
        if self.out_of_screen(size){
            // repos
            let mut npos = self.get_pos();
            if npos[0] < 0.{
                npos[0] = 0.;
            }
            if npos[0] > size[0]{
                npos[0] = size[0];
            }
            if npos[1] < 0.{
                npos[1] = 0.
            }
            if npos[1] > size[0]{
                npos[1] = size[1];
            }
            self.set_pos(npos);

            // change speed
            let mut nspeed = perp(self.get_speed());
            npos = add(npos, nspeed);
            if out_of_screen(self.get_hbox(), npos, size){
                nspeed = sub([0., 0.], nspeed);
            }
            self.set_speed(mul_scalar(nspeed, 0.6));
        }
    }
}

/// An object with interactions with the environement
#[derive(Clone)]
pub struct Object{
    pos: Cell<Vec2d>,
    speed: Cell<Vec2d>,
    direction: Cell<f64>,
    rota: Cell<f64>,
    hbox: HBox,
    graph : Graphic
}

impl Object{

    /// Create a new object
    pub fn new(pos: Vec2d, speed: Vec2d, dir: f64, rota: f64, hbox: HBox, graph: Graphic)->Object{
        Object{
            pos: Cell::new(pos),
            speed: Cell::new(speed),
            direction: Cell::new(dir),
            rota: Cell::new(rota),
            hbox: hbox,
            graph: graph
        }
    }

    /// Render the object
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        self.graph.render(args, gl, &self.get_pos(), &self.get_direction());
    }

    /// upadate object
    pub fn update(&self, args: &UpdateArgs){
        let pos = self.get_pos();
        let speed = self.get_speed();
        let dir = self.get_direction();
        let rota = self.get_rota();

        self.set_pos(add(pos, mul_scalar(speed, args.dt)));
        self.set_direction(dir + rota * args.dt);
    }
}

impl TObjectControler for Object{

    fn get_pos(&self)->Vec2d{
        self.pos.get()
    }

    fn set_pos(&self, pos: Vec2d){
        self.pos.set(pos)
    }

    fn get_speed(&self)->Vec2d{
        self.speed.get()
    }

    fn set_speed(&self, speed: Vec2d){
        self.speed.set(speed)
    }

    fn get_direction(&self)->f64{
        self.direction.get()
    }

    fn set_direction(&self, dir: f64){
        self.direction.set(dir)
    }

    fn get_rota(&self)->f64{
        self.rota.get()
    }

    fn set_rota(&self, rota: f64){
        self.rota.set(rota)
    }

    fn get_hbox(&self)->&HBox{
        &self.hbox
    }

    fn set_hbox(&mut self, hbox: HBox){
        self.hbox = hbox
    }

    fn get_graph(&self)->&Graphic{
        &self.graph
    }

    fn set_graph(&mut self, graph: Graphic){
        self.graph = graph
    }
}
