use std::collections::HashSet;

/// The score of a RAST run
pub struct RunGame{
    /* The run start timestamp */
    date: usize,
    /* The rue duration */
    duration: f64,
    /* The score make during the run */
    score: usize
}

impl RunGame{

    pub fn new(date: usize, duration: f64, score: usize)->RunGame{
        RunGame{
            date: date,
            duration: duration,
            score: score
        }
    }

    pub fn date(&self)->usize{
        self.date
    }

    pub fn duration(&self)->f64{
        self.duration
    }

    pub fn score(&self)->usize{
        self.score
    }
}

/// A RAST player
pub struct Player{
    /* Player name */
    name: String,
    /* Games history */
    games: Vec<RunGame>,
    /* player score */
    score: usize,
    /* remaining money */
    remain: usize,
    /* unlocked equipements */
    unlocked: HashSet<String>
}

impl Player{

    /// Create a new player
    pub fn new(name: String)->Player{
        Player{
            name: name,
            games: vec!(),
            score: 0,
            remain: 0,
            unlocked: HashSet::new()
        }
    }

    /// Add a game to the player
    pub fn add_game(&mut self, game: RunGame){
        self.score += game.score();
        self.remain += game.score();
        self.games.push(game)
    }

    /// Get the player name
    pub fn name(&self)->&String{
        &self.name
    }

    /// Get the player games history
    pub fn games(&self)->&Vec<RunGame>{
        &self.games
    }

    /// Get the player score
    pub fn score(&self)->usize{
        self.score
    }

    /// Get the remaining money
    pub fn remain(&self)->usize{
        self.remain
    }

    /// Test if the equipement is unlocked
    pub fn is_unlock(&self, equip: String)->bool{
        self.unlocked.contains(&equip)
    }

    /// Try to buy an equipement (only if the player has enougth money)
    pub fn buy(&mut self, equip: String, ammount: usize)->bool{
        let b = self.remain >= ammount;
        if b{
            self.remain -= ammount;
            self.unlocked.insert(equip);
        }

        b
    }

}
