use graphics::types::{Vec2d, Polygon};
use math::math::*;

/// Get the greatest radius to the center for a polygon
pub fn max_rad(poly: Polygon)->f64{
    let mut max = 0.;

    for p in poly{
        let l = square_len(*p);
        if l > max{
            max = l;
        }
    }

    max.sqrt()
}

/// Translate a polygon
pub fn trans_polygon(poly: &mut Vec<Vec2d>, trans: Vec2d){
    for i in 0..poly.len(){
        poly[i] = add(poly[i], trans);
    }
}

/// Create a circular polygon
pub fn circle_to_polygon(rad: f64, pos: Vec2d)->Vec<Vec2d>{
    let npts = 8;

    let mut poly = vec!();
    let steep = TPI / npts as f64;

    for i in 0..npts{
        let v = add(angle_to_vec(steep * i as f64, rad), pos);
        poly.push(v);
    }

    poly
}
