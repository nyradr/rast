use graphics::types::{Vec2d, Polygon};
use rand;

use math::math::*;

/// Hit box
#[derive(Clone)]
pub enum HBox{
    None,
    /// Circle hitbox
    Circle(f64),
    /// Single point
    Point(Vec2d),
    /// Polygonal hitbox
    Polygon(Vec<Vec2d>),
    /// Optimised hitbox
    Opti(Box<HBox>, Box<HBox>),
    /// Complex hitbox
    Complex(Vec<HBox>)
}

/// Test the collision between two circle
fn circle_circle_collide(pa: Vec2d, ra: f64, pb: Vec2d, rb: f64)->bool{
    let r = ra + rb;
    square_len(sub(pa, pb)) <= r * r
}

/// Test the collision between a circle and a point
pub fn circle_pt_collide(pa: Vec2d, ra: f64, pb: Vec2d)->bool{
    circle_circle_collide(pa, ra, pb, 0.)
}

/// Test if a point is in a polygon
pub fn poly_pt_collide(poly: Polygon, pp: Vec2d, dir: f64, pt: Vec2d) -> bool{
    let j = [-10000. - (rand::random::<f64>() % 100.), -10000. - (rand::random::<f64>() % 100.)];

    let mut ninter = 0;

    for i in 0..poly.len(){
        let pa = abso(poly[i], pp, dir);

        let pb: Vec2d;
        if i == poly.len() -1{
            pb = abso(poly[0], pp, dir);
        }else{
            pb = abso(poly[i +1], pp, dir);
        }

        let iseg = inter_seg(pa, pb, j, pt);

        if iseg == -1{
            //println!("DEGENERATE POLY {:?}-{:?} PT {:?}", pa, pb, pt);
            return poly_pt_collide(poly, pp, dir, pt);
        }

        ninter += iseg;
    }

    ninter % 2 == 1
}

/// Collision between a line and a circle
fn collide_line(pa: Vec2d, pb: Vec2d, pc: Vec2d, rc: f64)->bool{
    let u = sub(pb, pa);
    let ac = sub(pc, pa);

    let mut num = u[0] * ac[1] - u[1] * ac[0];
    if num < 0.{
        num = - num;
    }

    let denom = (u[0] * u[0] + u[1] * u[1]).sqrt();
    let ci = num / denom;

    ci < rc
}

/// Test the collision between a circle and a segment
pub fn collide_circle_seg(pa: Vec2d, pb: Vec2d, pc: Vec2d, rc: f64)->bool{
    if collide_line(pa, pb, pc, rc){

        let ab = sub(pb, pa);
        let ac = sub(pc, pa);
        let bc = sub(pc, pb);

        let d1 = ab[0] * ac[0] + ab[1] * ac[1];
        let d2 = (-ab[0]) * bc[1] + (-ab[1]) * bc[1];

        if d1 >= 0. && d2 >= 0.{
            return true;
        }

        if circle_pt_collide(pc, rc, pa){
            return true
        }

        if circle_pt_collide(pc, rc, pb){
            return true;
        }
    }

    false
}

/// Test the collsion between a polygon and a circle
fn poly_circle_collide<'a>(poly: Polygon, pp: Vec2d, dir: f64, pc: Vec2d, rc: f64)->bool{
    if poly_pt_collide(poly, pp, dir, pc){ // circle center in polygon
        return true;
    }

    for i in 0..poly.len(){
        let pa = abso(poly[i], pp, dir);
        let pb;
        if i == poly.len() -1{
            pb = abso(poly[0], pp, dir);
        }else{
            pb = abso(poly[i +1], pp, dir);
        }

        if collide_circle_seg(pa, pb, pc, rc){
            return true;
        }
    }

    false
}

/// Test the collsion between two polygons
fn poly_poly_collide<'a, 'b>(polya: Polygon, pa: Vec2d, dira: f64, polyb: Polygon, pb: Vec2d, dirb: f64) -> bool{
    for pt in polya.iter(){
        let apt = abso(*pt, pa, dira);
        let collide = poly_pt_collide(polyb, pb, dirb, apt);
        if collide{
            return true;
        }
    }

    false
}

/// Collision between an hitbox and a complex hitbox
fn hbox_complex_collide(ha: &HBox, pa: Vec2d, dira: f64, lhb: &Vec<HBox>, pb: Vec2d, dirb: f64)->bool{
    for ref hb in lhb.iter(){
        if collide(ha, pa, dira, hb, pb, dirb){
            return true;
        }
    }

    false
}

/// Collision between two complex hitbox
fn complex_complex_collide(lha: &Vec<HBox>, pa: Vec2d, dira: f64, lhb: &Vec<HBox>, pb: Vec2d, dirb: f64)->bool{
    for ref ha in lha.iter(){
        if hbox_complex_collide(ha, pa, dira, lhb, pb, dirb){
            return true;
        }
    }

    false
}

/// Detect the collsion of two hitbox
pub fn collide(ha: &HBox, pa: Vec2d, dira: f64, hb: &HBox, pb: Vec2d, dirb: f64)->bool{
    match ha{
        &HBox::None => false,
        &HBox::Circle(ra) => {
            match hb{
                &HBox::None => false,
                &HBox::Circle(rb) =>{
                    circle_circle_collide(pa, ra, pb, rb)
                },
                &HBox::Point(pt) => {
                    circle_pt_collide(pa, ra, add(pb, pt))
                }
                &HBox::Polygon(ref polyb) =>{
                    poly_circle_collide(polyb, pb, dirb, pa, ra)
                },
                &HBox::Opti(ref grb, ref smb) =>{
                    if collide(ha, pa, dira, grb, pb, dirb){
                        collide(ha, pa, dira, smb, pb, dirb)
                    }else{
                        false
                    }
                },
                &HBox::Complex(ref lhb) =>{
                    hbox_complex_collide(ha, pa, dira, lhb, pb, dirb)
                }
            }
        },
        &HBox::Point(pt) => {
            let apt = add(pa, pt);

            match hb{
                &HBox::None => false,
                &HBox::Circle(rb) =>{
                    circle_pt_collide(pb, rb, apt)
                },
                &HBox::Point(ptb) => {
                    let aptb = add(pb, ptb);
                    apt == aptb
                },
                &HBox::Polygon(ref polyb) =>{
                    poly_pt_collide(polyb, pb, dirb, apt)
                },
                &HBox::Opti(ref grb, ref smb) =>{
                    if collide(ha, pa, dira, grb, pb, dirb){
                        collide(ha, pa, dira, smb, pb, dirb)
                    }else{
                        false
                    }
                },
                &HBox::Complex(ref lhb) =>{
                    hbox_complex_collide(ha, pa, dira, lhb, pb, dirb)
                }
            }
        }
        &HBox::Polygon(ref polya) => {
            match hb{
                &HBox::None => false,
                &HBox::Circle(rb) =>{
                    poly_circle_collide(polya, pa, dira, pb, rb)
                },
                &HBox::Point(pt) =>{
                    poly_pt_collide(polya, pa, dira, add(pb, pt))
                }
                &HBox::Polygon(ref polyb) =>{
                    poly_poly_collide(polya, pa, dira, polyb, pb, dirb)
                },
                &HBox::Opti(ref grb, ref smb) =>{
                    if collide(ha, pa, dira, grb, pb, dirb){
                        collide(ha, pa, dira, smb, pb, dirb)
                    }else{
                        false
                    }
                },
                &HBox::Complex(ref lhb) =>{
                    hbox_complex_collide(ha, pa, dira, lhb, pb, dirb)
                }
            }
        },
        &HBox::Opti(ref gra, ref sma) =>{
            match hb{
                &HBox::None => false,
                &HBox::Opti(ref grb, ref smb) =>{
                    if collide(gra, pa, dira, grb, pb, dirb){
                        collide(sma, pa, dira, smb, pb, dirb)
                    }else{
                        false
                    }
                },
                _ =>{
                    if collide(gra, pa, dira, hb, pb, dirb){
                        collide(sma, pa, dira, hb, pb, dirb)
                    }else{
                        false
                    }
                }
            }
        },
        &HBox::Complex(ref lha) => {
            match hb{
                &HBox::None => false,
                &HBox::Complex(ref lhb) =>{
                    complex_complex_collide(lha, pa, dira, lhb, pb, dirb)
                },
                &HBox::Opti(ref grb, ref smb) =>{
                    if collide(ha, pa, dira, grb, pb, dirb){
                        collide(ha, pa, dira, smb, pb, dirb)
                    }else{
                        false
                    }
                },
                _ =>{
                    hbox_complex_collide(hb, pb, dirb, lha, pa, dira)
                }
            }
        }
    }
}

/// Test if the hitbox is out of screen
pub fn out_of_screen(_: &HBox, pa: Vec2d, size: Vec2d)->bool{
    pa[0] < 0. || pa[0] > size[0] || pa[1] < 0. || pa[1] > size[1]
}
