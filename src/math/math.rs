use graphics::types::Vec2d;

pub use graphics::math::*;
pub use std::f64::consts::PI;

pub const TPI:f64 = 2. * PI;

/// Test the intersection of two segment
pub fn inter_seg(pa: Vec2d, pb: Vec2d, pi: Vec2d, pp: Vec2d)->i8{
    let d = sub(pb, pa);
    let e = sub(pp, pi);

    let denom = d[0] * e[1] - d[1] * e[0];
    if denom == 0.{
        return -1;
    }
    let t = -(pa[0] * e[1] - pi[0] * e[1] - e[0] * pa[1] + e[0] * pi[1]) / denom;

    if t < 0. || t >= 1.{
        return 0;
    }

    let u = -(-d[0] * pa[1] + d[0] * pi[1] + d[1] * pa[0] - d[1] * pi[0]) / denom;

    if u < 0. || u >= 1.{
        return 0;
    }

    1
}

/// Get the intersection point of the lines D(pa, pb) and E(pc, pd)
pub fn pt_inter(pa: Vec2d, pb: Vec2d, pc: Vec2d, pd: Vec2d)->Vec2d{
    let da = (pb[1] - pa[1]) / (pb[0] - pa[0]);
    let db = pa[1] - da * pa[0];
    let ea = (pd[1] - pc[1]) / (pd[0] - pc[0]);
    let eb = pc[1] - da * pc[0];

    let xc = (eb - db) / (da - ea);
    let yc = da * xc + db;
    [xc, yc]
}

/// Get the intersection point of two segment (if exist)
pub fn intersect(pa: Vec2d, pb: Vec2d, pc: Vec2d, pd: Vec2d)->Option<Vec2d>{
    if inter_seg(pa, pb, pc, pd) == 1{
        Some(pt_inter(pa, pc, pc, pd))
    }else{
        None
    }
}

/// Get the modulus of a vector to another vector
pub fn mod_vec(v: Vec2d, m: Vec2d)->Vec2d{
    [v[0] % m[0], v[1] % m[1]]
}

/// Get a vector from an angle and a radius
pub fn angle_to_vec(dir: f64, radius: f64)->Vec2d{
    [dir.cos() * radius, dir.sin() * radius]
}

/// Get the angle of a vector
pub fn orientation(v : Vec2d)->f64{
    v[1].atan2(v[0])
}

/// Get the orientation of the pos point relativly to rel
pub fn rel_orient(rel: Vec2d, pos: Vec2d)->f64{
    orientation(sub(rel, pos))
}

/// Rotate a vector to a given direction
pub fn rotate(pa: Vec2d, dir: f64)->Vec2d{
    let cos = dir.cos();
    let sin = dir.sin();

    [pa[0] * cos - pa[1] * sin, pa[0] * sin + pa[1] * cos]
}

/// Get the absolute position of pa
pub fn abso(pa: Vec2d, pp: Vec2d, dir: f64) -> Vec2d{
    let r = rotate(pa, dir);
    add(r, pp)
}
