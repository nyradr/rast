use std::collections::HashSet;

use piston::input::*;
use graphics::types::Vec2d;

/// Inputs from the user
pub struct Inputs{
    /// Buttons currently pressed
    btns: HashSet<Button>,
    /// Mouse current point
    pt: Vec2d
}

impl Inputs{
    pub fn new()->Self{
        Self{
            btns: HashSet::new(),
            pt: [0.; 2]
        }
    }

    /// Test if the button is pressed
    pub fn is_pressed(&self, button: &Button)->bool{
        self.btns.contains(button)
    }

    /// Get the mouse positon
    pub fn get_pt(&self)->Vec2d{
        self.pt
    }

    /// Mark the button as pressed
    pub fn button_press(&mut self, button: Button){
        self.btns.insert(button);
    }

    /// Unmark the button as pressed
    pub fn button_release(&mut self, button: Button){
        self.btns.remove(&button);
    }

    /// Set the mouse cursor point
    pub fn mouse_cursor(&mut self, pt: Vec2d){
        self.pt = pt;
    }
}
