use piston::input::*;
use opengl_graphics::GlGraphics;
use graphics::*;
use graphics::types::{Color, Vec2d};
use graphics::math::*;

use std::f64::consts::PI;

#[derive(Clone)]
pub enum Graphic{
    None,
    Circle(Vec2d, Color, f64),
    Polygon(Vec2d, Color, Vec<Vec2d>),
    Complex(Vec2d, Vec<Graphic>)
}

const REC: [f64; 4] = [0., 0., 0., 0.];

/// Render a circle
fn render_circle(args: &RenderArgs, gl: &mut GlGraphics, pos: &Vec2d, rel: &Vec2d, col: &Color, radius: &f64){
    gl.draw(args.viewport(), |c, gl|{
        let abso = add(*pos, *rel);
        let trans = c.transform.trans(abso[0], abso[1]);

        circle_arc(*col, *radius, 0., PI, REC, trans, gl);
        circle_arc(*col, *radius, PI, 0., REC, trans, gl);
    });
}

/// Render a polygon
fn render_polygon(args: &RenderArgs, gl: &mut GlGraphics, pos: &Vec2d, rota: &f64, rel: &Vec2d, col: &Color, poly: &Vec<Vec2d>){
    gl.draw(args.viewport(), |c, gl|{
        let abso = add(*pos, *rel);
        let trans = c.transform
            .trans(abso[0], abso[1])
            .rot_rad(*rota);

        polygon(*col, poly, trans, gl);
    });
}

/// Render compex grahpics
fn render_complex(args: &RenderArgs, gl: &mut GlGraphics, pos: &Vec2d, rota: &f64, rel: &Vec2d, objs: &Vec<Graphic>){
    let npos = add(*pos, *rel);
    for ref gr in objs{
        gr.render(args, gl, &npos, rota);
    }
}

impl Graphic{

    /// Render a grahpics
    pub fn render(&self, args: &RenderArgs, gl: &mut GlGraphics, pos: &Vec2d, rota: &f64){
        match self{
            &Graphic::None => {},
            &Graphic::Circle(ref rel, ref col, ref radius) => render_circle(args, gl, pos, rel, col, radius),
            &Graphic::Polygon(ref rel, ref col, ref poly) => render_polygon(args, gl, pos, rota, rel, col, poly),
            &Graphic::Complex(ref rel, ref objs) => render_complex(args, gl, pos, rota, rel, objs)
        }
    }
}
