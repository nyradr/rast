extern crate piston;
extern crate graphics;
extern crate opengl_graphics;
extern crate touch_visualizer;
extern crate rand;
extern crate clipping;

#[cfg(feature = "include_sdl2")]
extern crate sdl2_window;
#[cfg(feature = "include_glfw")]
extern crate glfw_window;
#[cfg(feature = "include_glutin")]
extern crate glutin_window;

mod math;
mod graphs;
mod object;
#[allow(warnings)]
mod player;
mod level;
mod rast;
mod inputs;

use rast::Rast;

fn main() {
    let mut rast = Rast::new();
    rast.run();
}
